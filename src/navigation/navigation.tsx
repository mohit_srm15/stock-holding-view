
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { MyHoldings } from '../screens';
import { SCREEN_NAMES } from './constants';

const Stack = createNativeStackNavigator();

export const NavigationStackContainer = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name={SCREEN_NAMES.MY_HOLDINGS} component={MyHoldings} options={{headerShown: false}} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default NavigationStackContainer;