import React from 'react';
import {View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

import {useGetMyHoldings} from '../../api/apiHooks/myHoldings';
import {Header, HoldingList, PortfolioSummary} from './components';
import { styles } from './myHoldings.styles';

export const MyHoldings = () => {
  const {isLoading, data, error} = useGetMyHoldings();
  return (
    <SafeAreaView style={styles.flex}>
      <Header title="Upstox Holding" />
      <View style={styles.flex}>
        <HoldingList holdingList={data} />
      </View>
      <PortfolioSummary holdingList={data} />
    </SafeAreaView>
  );
};
