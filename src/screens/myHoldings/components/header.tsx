import React from 'react';
import {StyleSheet, View, ViewStyle, Text} from 'react-native';

type HeaderProps = {
  title: string;
  containerStyle?: ViewStyle;
};

export const Header = (props: HeaderProps) => {
  const {title, containerStyle = {}} = props;

  return (
    <View style={StyleSheet.flatten([styles.contianer, containerStyle])}>
      <Text style={styles.titleText}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  contianer: {
    height: 40,
    backgroundColor: 'purple',
    paddingHorizontal: 16,
    justifyContent: 'center',
  },
  titleText: {
    color: 'white',
  },
});
