import React, {useCallback, useRef} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';

import {HoldingType} from '../../../api/apiHooks/myHoldings';
import {
  converAmountToDecimalPlaces,
  getPnlValue,
} from '../../../utils/myHoldingHelper';

type HoldingListProps = {
  holdingList: HoldingType[];
};

export const HoldingList = (props: HoldingListProps) => {
  const {holdingList = []} = props;

  const renderItem = useCallback(({item}: {item: HoldingType}) => {
    const {ltp, avgPrice, symbol, quantity} = item;
    const pnlValue = getPnlValue(ltp, avgPrice, quantity);
    return (
      <View style={styles.itemContainer}>
        <View>
          <Text style={styles.stockName}>{symbol}</Text>
          <Text style={styles.stockQuantity}>{quantity}</Text>
        </View>
        <View style={styles.amountContainer}>
          <Text style={styles.ltpTitle}>
            {'LTP: ₹ '}
            <Text style={styles.ltpAmount}>
              {' '}
              {converAmountToDecimalPlaces(ltp)}
            </Text>
          </Text>
          <Text style={styles.pnlTitle}>
            {'P/L: ₹ '}
            <Text style={styles.ltpAmount}>
              {converAmountToDecimalPlaces(pnlValue)}
            </Text>
          </Text>
        </View>
      </View>
    );
  }, []);

  const keyExtractor = useRef((item: HoldingType) => item.symbol).current;
  const ItemSeparator = useRef(() => (
    <View style={styles.itemSeparator} />
  )).current;

  return (
    <FlatList
      data={holdingList}
      renderItem={renderItem}
      keyExtractor={keyExtractor}
      ItemSeparatorComponent={ItemSeparator}
    />
  );
};

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 50,
    padding: 10,
    alignItems: 'center',
  },
  stockName: {fontWeight: 'bold', fontSize: 12},
  stockQuantity: {fontSize: 12, marginTop: 4},
  amountContainer: {alignItems: 'flex-end'},
  ltpTitle: {fontSize: 12},
  ltpAmount: {fontWeight: 'bold'},
  pnlTitle: {fontSize: 12, marginTop: 4},
  itemSeparator: {backgroundColor: 'lightgrey', height: 1},
});
