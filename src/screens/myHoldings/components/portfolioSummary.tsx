import React, {useCallback, useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

import {HoldingType} from '../../../api/apiHooks/myHoldings';
import {
  converAmountToDecimalPlaces,
  getCurrentValue,
  getInvestmentValue,
} from '../../../utils/myHoldingHelper';

type PortfolioSummaryProps = {
  holdingList: HoldingType[];
};
const expandedViewElements = [
  {
    title: 'Current Value:',
    amount: 0,
  },
  {
    title: 'Total Investment',
    amount: 0,
  },
  {
    title: "Today's Profit and Loss",
    amount: 0,
  },
];

export const PortfolioSummary = (props: PortfolioSummaryProps) => {
  const {holdingList} = props;

  const [totalPnlAmount, setTotalPnlAmount] = useState(0);
  const [isVisible, setIsVisible] = useState(false);

  const handleHideCollapseView = useCallback(
    () => setIsVisible(!isVisible),
    [isVisible],
  );

  const getPortfolioData = () => {
    let currentValueTotal = 0,
      investmentValuetotal = 0,
      todayPnlValueTotal = 0;
    holdingList.forEach(stock => {
      currentValueTotal =
        currentValueTotal + getCurrentValue(stock.ltp, stock.quantity);
      investmentValuetotal =
        investmentValuetotal +
        getInvestmentValue(stock.avgPrice, stock.quantity);
      todayPnlValueTotal =
        todayPnlValueTotal + (stock.close - stock.ltp) * stock.quantity;
    });
    expandedViewElements[0].amount = currentValueTotal;
    expandedViewElements[1].amount = investmentValuetotal;
    expandedViewElements[2].amount = todayPnlValueTotal;
    setTotalPnlAmount(currentValueTotal - investmentValuetotal);
  };

  useEffect(() => {
    getPortfolioData();
  }, [holdingList]);

  const ExpandedView = useCallback(() => {
    return (
      <View style={styles.expandedViewContainer}>
        {expandedViewElements.map((element, index) => (
          <View
            key={`${element}-${index}`}
            style={styles.expandedItemTitleContainer}>
            <Text style={styles.expandedItemTitle}>
              {element.title}
            </Text>
            <Text style={styles.expandedItemAmount}>
              {'₹ '}
              {converAmountToDecimalPlaces(element.amount)}
            </Text>
          </View>
        ))}
      </View>
    );
  }, []);

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={handleHideCollapseView}
        style={styles.expandCollapseButton}>
        <Text style={styles.buttonTitle}>
          {`Click To ${isVisible ? 'collapse' : 'expand'}`}
        </Text>
      </TouchableOpacity>
      {isVisible ? <ExpandedView /> : null}
      <View style={styles.pnlTitleContainer}>
        <Text style={styles.pnlTitle}>{'Profit and Loss'}</Text>
        <Text style={styles.pnlAmount}>
          {'₹ '}
          {converAmountToDecimalPlaces(totalPnlAmount)}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    paddingVertical: 12,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    backgroundColor: 'lightgrey',
  },
  expandCollapseButton: {
    borderColor: 'purple',
    padding: 10,
    borderWidth: 1,
    borderRadius: 10,
    maxWidth: 150,
    alignItems: 'center',
    alignSelf: 'center',
  },
  buttonTitle: {fontSize: 12, fontWeight: 'bold', color: 'purple'},
  pnlTitleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 6,
    justifyContent: 'space-between',
  },
  pnlTitle: {fontSize: 14, fontWeight: 'bold'},
  pnlAmount: {fontSize: 12},
  expandedViewContainer: {paddingBottom: 12},
  expandedItemTitleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 6,
    justifyContent: 'space-between',
  },
  expandedItemTitle: {fontSize: 14, fontWeight: 'bold'},
  expandedItemAmount: {fontSize: 12}
});
