export const getCurrentValue = (ltp: number, quantity: number) => ltp * quantity;

export const getInvestmentValue = (avgPrice: number, quantity: number) => avgPrice * quantity;

export const getPnlValue = (ltp: number, avgPrice: number, quantity: number) => {
    const currentValue = getCurrentValue(ltp, quantity);
    const investmentValue = getInvestmentValue(avgPrice, quantity);
    return (currentValue - investmentValue);
}

export const converAmountToDecimalPlaces = (amount: number, decimalPlaces: number = 3) => amount %1 === 0  ? amount: amount.toFixed(decimalPlaces);