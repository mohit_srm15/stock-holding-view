import React, { useEffect, useState } from 'react';
import { API_ENDPOINTS } from '..';

export type HoldingType = {
    symbol: string,
    quantity: number,
    ltp: number,
    avgPrice: number,
    close: number
}

type responseType = {
    data: HoldingType[],
    error: any,
    isLoading: boolean
}


const initialData: responseType = {
    data: [],
    error: '',
    isLoading: false
}

export const useGetMyHoldings = () => {
    const [holdingResp, setHoldingApiResp] = useState<responseType>(initialData);

    const getMyHoldingList = () => {
        setHoldingApiResp({...holdingResp, isLoading: true});
        fetch(API_ENDPOINTS.getMyHoldings).then(res => res.json()).then(res => {
            console.log('res', res);
            setHoldingApiResp({...holdingResp, data: res?.userHolding ?? [], isLoading: false});
        }).catch((error) => setHoldingApiResp({...holdingResp, error, isLoading: false}));
    }
    

    useEffect(() => {
        getMyHoldingList();
    }, [])


    return {...holdingResp}
}