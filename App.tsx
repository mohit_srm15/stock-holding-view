/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {View} from 'react-native';
import {NavigationStackContainer} from './src/navigation';

function App(): React.JSX.Element {
  return (
    <View style={{flex: 1}}>
      <NavigationStackContainer />
    </View>
  );
}

export default App;
